<?php

namespace App\Entity;

use App\Repository\AuthorRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @ORM\Entity(repositoryClass=AuthorRepository::class)
 */
#[ApiResource(
    collectionOperations:["post"=>["normalization_context"=>['groups'=>["write:author","read:author"]]],"get" =>["normalization_context"=>['groups'=>["write:author","read:author"]]]],
    itemOperations:["get" =>["normalization_context"=>['groups'=>["write:author","read:author"]]]
    ,"put","delete"],
    normalizationContext:["groups"=>["read:author"]],
    denormalizationContext:["groups"=>["write:author"]]
    )]
class Author
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("read:author")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("write:author")
     */
    private $name;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups("write:author")
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("write:author")
     */
    private $birthPlace;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups("write:author")
     */
    private $deathDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("write:author")
     */
    private $deathPlace;

    /**
     * @ORM\OneToMany(targetEntity=Book::class, mappedBy="author", orphanRemoval=true)
     * @Groups("read:author")
     */
    private $books;

    /**
     * @ORM\Column(type="integer")
     * @Groups("write:author")
     */

    private $age;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    // public function getage($deathDate): ?int
    // {
    //     $age = date('Y')-date('Y',strtotime($deathDate));
    //     return $this->age;
    // }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getBirthPlace(): ?string
    {
        return $this->birthPlace;
    }

    public function setBirthPlace(?string $birthPlace): self
    {
        $this->birthPlace = $birthPlace;

        return $this;
    }

    public function getDeathDate(): ?\DateTimeInterface
    {
        return $this->deathDate;
    }

    public function setDeathDate(?\DateTimeInterface $deathDate): self
    {
        $this->deathDate = $deathDate;

        return $this;
    }

    public function getDeathPlace(): ?string
    {
        return $this->deathPlace;
    }

    public function setDeathPlace(?string $deathPlace): self
    {
        $this->deathPlace = $deathPlace;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->setAuthor($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->removeElement($book)) {
            // set the owning side to null (unless already changed)
            if ($book->getAuthor() === $this) {
                $book->setAuthor(null);
            }
        }

        return $this;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }
}
