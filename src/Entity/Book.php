<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BookRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 */
#[ApiResource(
    collectionOperations:["post"=>["normalization_context"=>['groups'=>["write:book","read:book"]]],"get" =>["normalization_context"=>['groups'=>["write:book","read:book"]]]],
    itemOperations:["get" =>["normalization_context"=>['groups'=>["write:book","read:book"]]]
    ,"put","delete"],
    normalizationContext:["groups"=>["read:book"]],
    denormalizationContext:["groups"=>["write:book"]]
    )]
class Book
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("read:book")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("write:book")
     */

    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("read:book")
     */
    private $publicationDate;

    /**
     * @ORM\ManyToOne(targetEntity=Author::class, inversedBy="books")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("write:book")
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity=Genre::class, inversedBy="books")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("write:book")
     */
    private $genre;
    public function __construct()
    {
        $this->publicationDate= new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPublicationDate(): ?\DateTimeInterface
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(\DateTimeInterface $publicationDate): self
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getGenre(): ?Genre
    {
        return $this->genre;
    }

    public function setGenre(?Genre $genre): self
    {
        $this->genre = $genre;

        return $this;
    }
}
